#!/bin/sh

BASEDIR=/usr/share/alpine-mirror-status
MIRRORDIR=/var/lib/mirrors
WEBDIR=/var/www/localhost/htdocs

exec > /proc/1/fd/1 2>&1

if [ -d "$MIRRORDIR" ]; then
	cd "$MIRRORDIR" && git pull
else
	export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no"
	git clone git@gitlab.alpinelinux.org:alpine/infra/mirrors.git "$MIRRORDIR"
fi

cd "$BASEDIR"
./generate-mirrors-json.lua "$MIRRORDIR"/private.yaml "$WEBDIR"
./generate-json.lua debug && ./generate-html.lua
