FROM alpinelinux/mqtt-exec

ENV NQDIR=/tmp

SHELL [ "/bin/ash", "-eo", "pipefail", "-c" ]

RUN apk add --no-cache git \
	openssh-client \
	nq \
	lua5.3-cjson \
	lua5.3-http \
	lua5.3-lustache \
	lua5.3-lyaml \
	lua-penlight && \
	mkdir -p /usr/share/alpine-mirror-status /var/www/localhost/htdocs && \
	wget -qO- https://gitlab.alpinelinux.org/alpine/infra/mirror-status/-/archive/master/mirror-status-master.tar.gz | \
	tar -zx --strip-components=1 -C /usr/share/alpine-mirror-status

COPY scripts /usr/local/bin

CMD [ "/usr/bin/nq", "update.sh" ]
